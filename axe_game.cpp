#include "raylib.h"

bool check_collision_with_axe(int circle_x, int circle_y, int circle_radius, int axe_x, int axe_y, int axe_length)
{
    // cirlce edges
    int l_cirlce_x{circle_x - circle_radius};
    int r_cirlce_x{circle_x + circle_radius};
    int u_cirlce_y{circle_y - circle_radius};
    int b_cirlce_y{circle_y + circle_radius};

    // axe edges
    int l_axe_x{axe_x};
    int r_axe_x{axe_x + axe_length};
    int u_axe_y{axe_y};
    int b_axe_y{axe_y + axe_length};

    bool collision_with_axe =
        b_axe_y >= u_cirlce_y &&
        u_axe_y <= b_cirlce_y &&
        r_axe_x >= l_cirlce_x &&
        l_axe_x <= r_cirlce_x;

    return collision_with_axe;
}

int main()
{
    // window dimensions
    int width{800};
    int height{450};
    InitWindow(width, height, "MRWN's Window");

    // circle coordinates
    int circle_x_start_position{200};
    int circle_y_start_position{200};
    int circle_x{circle_x_start_position};
    int circle_y{circle_y_start_position};
    int circle_radius{25};
    int cirlce_move_speed{10};

    int axe_x{400};
    int axe_y{0};
    int axe_length{50};

    int direction{10};

    SetTargetFPS(60);

    while (!WindowShouldClose())
    {
        BeginDrawing();
        ClearBackground(WHITE);

        // Check collision
        bool collision_with_axe = check_collision_with_axe(circle_x, circle_y, circle_radius, axe_x, axe_y, axe_length);
        if (collision_with_axe)
        {
            DrawText("Game Over! Press Space to restart", width / 2, height / 2, 20, RED);
            if (IsKeyDown(KEY_SPACE))
            {
                circle_x = circle_x_start_position;
                circle_y = circle_y_start_position;
                collision_with_axe = false;
            }
        }
        else
        {
            // Game logic begins

            DrawCircle(circle_x, circle_y, circle_radius, BLUE);
            DrawRectangle(axe_x, axe_y, axe_length, axe_length, RED);

            // move the axe
            axe_y += direction;
            if (axe_y > height - axe_length || axe_y < 0)
            {
                direction = -direction;
            }

            if (IsKeyDown(KEY_D) && circle_x < width - circle_radius)
            {
                circle_x += cirlce_move_speed;
            }

            if (IsKeyDown(KEY_A) && circle_x > circle_radius)
            {
                circle_x -= cirlce_move_speed;
            }

            if (IsKeyDown(KEY_S) && circle_y < height - circle_radius)
            {
                circle_y += cirlce_move_speed;
            }

            if (IsKeyDown(KEY_W) && circle_y > circle_radius)
            {
                circle_y -= cirlce_move_speed;
            }

            // Game logic ends
        }
        EndDrawing();
    }
}
